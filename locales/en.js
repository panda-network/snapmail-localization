// Copyright 2014 Startup Kitchen. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS-IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

goog.provide('app.locale.en');
goog.require('tart.locale');



tart.locale['en'] = {
    '_name': 'English',
    '{0} doesn\'t use Snapmail mobile. Your messages will be delivered by a self-destructing email. You can also invite {1} for using Snapmail Client.': '{0} doesn\'t use Snapmail mobile. Your messages will be delivered by a self-destructing email. You can also invite {1} for using Snapmail Client.',
    'About': 'About',
    'Chats': 'Chats',
    'Contacts': 'Contacts',
    'Favorites': 'Favorites',
    'Invite': 'Invite',
    'Invitation sent.': 'Invitation sent.',
    'Message can not be sent.': 'Message can not be sent.',
    'minutes': 'minutes',
    'Search {0} contacts': 'Search {0} contacts',
    'seconds': 'seconds',
    'Send': 'Send',
    'Settings': 'Settings',
    'Sign in with google': 'Sign in with google',
    'Sign out': 'Sign out',
    'Snapmail sent.': 'Snapmail sent.',
    'Time to self-destruct messages': 'Time to self-destruct messages',
    'Time to self-destruct threads': 'Time to self-destruct threads',
    'To start messaging securely please sign in with google': 'To start messaging securely please sign in with google',
    'You don\'t have any favorite.': 'You don\'t have any favorite.'
};
