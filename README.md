snapmail-localization
==

We're looking for open-source contributors to Snapmail's new mobile application! You can easily create a new locale file and add it to `/locales` folder. 

**Example:**

Assume you are translating Snapmail to French. 

Copy `/locales/en.js` change `goog.provide('app.locale.en')` to `goog.provide('app.locale.fr')`.


Change `tart.locale['en']` to `tar.locale['fr']` and start translating the new Snapmail application to French! 
